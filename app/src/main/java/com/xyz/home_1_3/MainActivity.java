package com.xyz.home_1_3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
/*

3. Создать приложение с одним экраном. 5 полей для ввода (EditText), текстовое поле (TextView) и
одну кнопку (Button). Поля (логин, email, номер телефона, пароль, повторный пароль) разместить по центру,
друг под другом, кнопку и текстовое поле - внизу экрана. По нажатию на кнопку реализовать валидацию полей
(должны быть заполнены все поля, пароли должны совпадать). Уведомить пользователя о результате
в текстовом поле ("валидация пройдена", "введите логин", "пароли не совпадают" пр.)
3.1* Проверять валидность email и номера телефона в международном формате (+380501234567)

 */

public class MainActivity extends AppCompatActivity {

    EditText login;
    EditText email;
    EditText phone;
    EditText pass;
    EditText passRetype;
    TextView textCheck;
    Button checkButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        login = (EditText) findViewById(R.id.editTextLogin);
        email = (EditText) findViewById(R.id.editTextEmail);
        phone = (EditText) findViewById(R.id.editTextPhone);
        pass = (EditText) findViewById(R.id.editTextPass);
        passRetype = (EditText) findViewById(R.id.editTextPassRetype);

        textCheck = (TextView) findViewById(R.id.textViewCheck);
        checkButton = (Button) findViewById(R.id.button_check);

        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (login.getText().toString().isEmpty()) {
                    textCheck.setText("Input login");
                    login.requestFocus();
                    return;
                }
                if (email.getText().toString().isEmpty()) {
                    textCheck.setText("Input email");
                    email.requestFocus();
                    return;
                }
                if (phone.getText().toString().isEmpty()) {
                    textCheck.setText("Input phone");
                    phone.requestFocus();
                    return;
                }
                if (pass.getText().toString().isEmpty()) {
                    textCheck.setText("Input password");
                    pass.requestFocus();
                    return;
                }
                if (passRetype.getText().toString().isEmpty()) {
                    textCheck.setText("Retype password");
                    passRetype.requestFocus();
                    return;
                }

                if (!pass.getText().toString().equals(passRetype.getText().toString())) {
                    textCheck.setText("Passwords mismatch!");
                    passRetype.requestFocus();
                    return;
                }

                textCheck.setText("Check done!");


            }
        });


    }
}
